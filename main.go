package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

const temperature string = "temp"
const fan string = "fan"
const pathToHwMon2 = "/sys/devices/platform/it87.656/hwmon/hwmon2/"
const pathToHwMon3 = "/sys/devices/platform/it87.656/hwmon/hwmon3/"

func exists(filePath string) (exists bool) {
	exists = true

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		exists = false
	}

	return
}

func readData(path string) (value string, err error) {

	data, err := ioutil.ReadFile(path)

	if err != nil {
		return
	}
	value = string(data)
	return
}

func main() {

	var pathToHwMon = pathToHwMon2
	if exists(pathToHwMon3) {
		pathToHwMon = pathToHwMon3
	}
	itemNumber := flag.Int("itemNumber", 1, "This is the number of the item to fetch.")
	itemType := flag.String("itemType", "", "This is the item type to fetch.")
	flag.Parse()
	pathToValue := pathToHwMon + *itemType + strconv.Itoa(*itemNumber) + "_input"

	value, err := readData(pathToValue)

	if err != nil {
		fmt.Println(err)
		return
	}

	value = strings.TrimSuffix(value, "\n")

	if *itemType == "temp" {
		numericValue, err := strconv.Atoi(value)

		if err != nil {
			fmt.Println(err)
		}
		numericValue = numericValue / 1000
		value = strconv.Itoa(numericValue)
	}

	fmt.Println(value)
}
